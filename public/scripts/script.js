jQuery( function($) {
    var socket = io.connect();
    var $nickForm = $('#setNick');
    var $nickError= $('#nickError');
    var $nickBox = $('#nickname');
    var $messageForm = $('#sendMessage');
    var $messageBox = $('#message');
    var $users = $('#users');
    var $chat = $('#chat');

    $messageForm.submit(function(e){
        e.preventDefault();
        socket.emit('send message',$messageBox.val(), function(data){
            $chat.append('<span class ="error">'+data+ '<span><br/>');
        });
        $messageBox.val('');
    });

    $nickForm.submit( function(e){
        console.log("smm")
        e.preventDefault();
        socket.emit('new user',$nickBox.val(), function(data) {
            //callback
            if(data) {
                $('#nickWrap').hide();
                $('#contentWrap').show();
            }
            else {
                $nickError.html('That username is already taken');
            }
        })
        $nickBox.val('');
    });

    socket.on('usernames', function(data) {
        var html = '';
        for(var i=0;i<data.length;i++) {
            html += data[i]+'<br />'
        }
        $users.html(html);
    });

    socket.on('load old msgs', function(docs){
        for(var i=docs.length - 1; i>=0; i--) {
            console.log('sd')
            displayMsg(docs[i]);
        }
    });

    function displayMsg(data){
        $chat.append('<b>'+data.user+'</b>: '+data.msg + '<br/>');
    }
    socket.on('new message', function(data) {
        console.log('new',data)
        displayMsg(data);
    });

    socket.on('whisper', function(data) {
        $chat.append('<span class ="whisper"><b>'+data.user+'</b>: '+data.msg + '<span><br/>');
    });
})
