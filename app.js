var express = require('express'),
    app = express(),
    server=require('http').createServer(app),
    io=require('socket.io').listen(server),
    mongoose=require('mongoose'),
    users = {};
var path = require('path');

server.listen(1200);
console.log("server listens on 1200")

// app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

var chatSchema = mongoose.Schema({
    user: String,
    msg: String,
    created: {type: Date, default: Date.now}
});

var Chat = mongoose.model('Message',chatSchema);





mongoose.connect('mongodb://localhost/chat', function(err){
    if(err) console.log(err);
    else {
        console.log("mongo connected!");
    }
})

app.get('/',function(req,res) {
    res.sendfile(__dirname + '/index.html')
});

io.sockets.on('connection', function(socket) {

    var query = Chat.find({})
    query.sort('-created').limit(8).exec( function(err,docs){
        if(err) throw err;
        console.log('sending old messges',docs);
        socket.emit('load old msgs', docs)
    })

    socket.on('new user', function(data, callback) {
        if(data in users) {
            callback(false);
        }
        else{
            callback(true);
            socket.nickname = data;
            users[socket.nickname] = socket; //using nickname as key and saving socket in there
            io.sockets.emit('usernames',Object.keys(users));
        }
    });

    socket.on('send message', function(data, callback) {
        var msg = data.trim();
        if( msg.substr(0,3) === '/w ' ) {
            msg = msg.substr(3);
            var ind = msg.indexOf(' ');
            if(ind !== -1) {
                var name = msg.substr(0,ind);
                var msg = msg.substr(ind+1);

                if(name in users) {
                    users[name].emit('whisper', { msg: msg, user: socket.nickname })
                } else {
                    console.log('not user')
                    callback('error! no such username');
                }
            } else {
                console.log('not messegr')
                callback('error! plz enter message to your whisper');
            }
        } else {
            var newMsg = new Chat({ msg: msg, user: socket.nickname });
            newMsg.save(function(err){
                if(err) throw err;
                io.sockets.emit('new message', { msg: msg, user: socket.nickname });
            })
        }
    });

    socket.on('disconnect', function(data) {
        if(!socket.nickname) return;
        delete users[socket.nickname]
        updateNicknames();
    })

    function updateNicknames() {
        io.sockets.emit('usernames', Object.keys(users));
    }


});
